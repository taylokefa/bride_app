import 'package:bridge_app/screen/pages/operations.dart';
import 'package:bridge_app/screen/pages/statistiques.dart';
import 'package:bridge_app/screen/pages/wallet.dart';
import 'package:flutter/material.dart';

import '../pages/compte.dart';

class ScreenDashboardRealPage extends StatefulWidget {
  const ScreenDashboardRealPage({super.key});

  @override

  State<ScreenDashboardRealPage> createState() => _ScreenDashboardRealPageState();
}

class _ScreenDashboardRealPageState extends State<ScreenDashboardRealPage> {

  int _selectedIndex = 0;

  final List<Widget> _pages = [
    ScreenWalletPage(), // These should be actual screens or widgets you navigate to
    ScreenOperationPage(),
    ScreenStatistiquePage(),
    ScreenComptePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFFFFFFF),
      body: IndexedStack(
        index: _selectedIndex,
        children: _pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            backgroundColor: Color(0XFF0000004D),
            icon: ImageIcon(AssetImage('assets/images/Wallet.png')),
            label: 'Portefeuilles',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/money.png')),
            label: 'Opérations',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/Statistiques.png')),
            label: 'Statistiques',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/bank-4@3x.png')),
            label: 'Compte',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0XFFFF3C00),
        onTap: _onItemTapped,
      ),
    );
  }
}
