import 'package:flutter/material.dart';

class ScreenHistorique extends StatefulWidget {
  const ScreenHistorique({super.key});

  @override
  State<ScreenHistorique> createState() => _ScreenHistoriqueState();
}

class _ScreenHistoriqueState extends State<ScreenHistorique> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Padding(
            padding: EdgeInsets.all(90.0),
            child: Text('Historique',
            style:  TextStyle(
                color: Color(0XFFF1F5F8),
              fontSize: 14,
              fontFamily: "Poppins_regular"),
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.notifications, color: Colors.white),
              onPressed: () {
                // Action pour les notifications
              },
            ),
          ],
          bottom: PreferredSize(
              preferredSize: Size.fromHeight(80.0),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    TextFormField(
                      autocorrect: false,
                      decoration: InputDecoration(
                          hintText: 'Recherche',
                          hintStyle: TextStyle(color: Colors.white),
                          prefixIcon: Icon(Icons.search, color: Colors.white,size: 22),
                          prefixIconConstraints: const BoxConstraints(
                            minWidth: 5,
                            minHeight: 5,
                          ),
                          isDense: true,
                          suffixIconConstraints: BoxConstraints(
                            minWidth: 2,
                            minHeight: 2,
                          ),
                          suffixIcon: InkWell(
                              child: Icon(Icons.filter_list_alt ,size: 22,color: Colors.white), onTap: () {}),
                        filled: true,
                        fillColor: Colors.grey[500],
                        border: OutlineInputBorder(
                        )
                      ),

                      //controller: _controller1,
                      maxLines: null,
                    ),

                    SizedBox(height: 8),
                  ],

                ),
              )),

          backgroundColor: Color(0XFF172530),

        ),

        body: SingleChildScrollView(
          child: Column(
            children: [
              TransactionHistory(),
          /*    Expanded(
                  child: ListView.builder(
                    itemCount: 10, // Nombre d'éléments dans la liste
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text('0707010203'),
                        subtitle: Text('TRANSFERT DE FONDS'),
                        trailing: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('32,080.00 FCFA'),
                            Text('10:23', style: TextStyle(color: Colors.grey)),
                          ],
                        ),
                      );
                    },
                
                  )
              ),*/
            ],
          ),
        ),
      ),
    );
  }
}

class TransactionHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              children: [

              ],
            ),
          ),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem1('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem1('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem1('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem1('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),



          // Ajoutez d'autres éléments de liste ici
        ],
      ),
    );
  }
}

class TransactionListItem extends StatelessWidget {
  final String transactionType;
  final String amount;
  final String additionalSubtitle;

  TransactionListItem(this.transactionType, this.amount,this.additionalSubtitle);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        transactionType,
        style: const TextStyle(fontSize: 16,color: Color(0XFF172530),fontFamily: "Poppins_regular",fontWeight: FontWeight.bold),
      ),
      subtitle: const Text('TRANSFERT DE FONDS',
      style: TextStyle(fontSize: 12,color: Color(0XFF172530), fontFamily: "Poppins-light"),),

      trailing: Column(
        children: [
          Text(
            amount,
            style: const TextStyle(fontSize: 14,color: Colors.green, fontWeight: FontWeight.bold,fontFamily: "Poppins_regular"),
          ),
          Text(
            additionalSubtitle, // Affichage du sous-titre supplémentaire en dessous du montant
            style: TextStyle(fontSize: 12,color: Color(0XFF172530), fontFamily: "Poppins-light"), // Vous pouvez personnaliser le style comme vous voulez
          ),
        ],
      ),
    );
  }
}

class TransactionListItem1 extends StatelessWidget {
  late final String transactionTypes;
  late final String amounts;
  final String additionalSubtitles;

  TransactionListItem1(this.transactionTypes, this.amounts,this.additionalSubtitles);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        transactionTypes,
        style: const TextStyle(fontSize: 16,color: Color(0XFF172530),fontFamily: "Poppins_regular",fontWeight: FontWeight.bold),
      ),
      subtitle: const Text('RETRAIT DE FONDS',
        style: TextStyle(fontSize: 12,color: Color(0XFF172530), fontFamily: "Poppins-light"),
      ),
      trailing: Column(
        children: [
          Text(
            amounts,
              style: const TextStyle(fontSize: 14,color: Colors.red, fontWeight: FontWeight.bold,fontFamily: "Poppins_regular")          ),
          Text(
            additionalSubtitles, // Affichage du sous-titre supplémentaire en dessous du montant
            style: TextStyle(fontSize: 12,color: Color(0XFF172530), fontFamily: "Poppins-light"), // Vous pouvez personnaliser le style comme vous voulez
          ),
        ],
      ),
    );
  }
}
