import 'package:flutter/material.dart';

class ScreenHistoriqueVirementPage extends StatefulWidget {
  const ScreenHistoriqueVirementPage({super.key});

  @override
  State<ScreenHistoriqueVirementPage> createState() => _ScreenHistoriqueVirementPageState();
}

class _ScreenHistoriqueVirementPageState extends State<ScreenHistoriqueVirementPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Padding(
            padding: EdgeInsets.all(90.0),
            child: Text('Historique',
              style:  TextStyle(
                  color: Color(0XFFF1F5F8),
                  fontSize: 14,
                  fontFamily: "Poppins_regular"),
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.notifications_none, color: Colors.white),
              onPressed: () {
                // Action pour les notifications
              },
            ),
          ],
          bottom: PreferredSize(
              preferredSize: Size.fromHeight(80.0),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    TextFormField(
                      autocorrect: false,
                      decoration: InputDecoration(
                          hintText: 'Recherche',
                          hintStyle: TextStyle(color: Colors.white),
                          prefixIcon: Icon(Icons.search, color: Colors.white,size: 22),
                          prefixIconConstraints: const BoxConstraints(
                            minWidth: 5,
                            minHeight: 5,
                          ),
                          isDense: true,
                          suffixIconConstraints: BoxConstraints(
                            minWidth: 2,
                            minHeight: 2,
                          ),
                          suffixIcon: InkWell(
                              child: Icon(Icons.filter_list_alt ,size: 22,color: Colors.white), onTap: () {}),
                          filled: true,
                          fillColor: Colors.grey[500],
                          border: OutlineInputBorder(
                          )
                      ),

                      //controller: _controller1,
                      maxLines: null,
                    ),

                    SizedBox(height: 8),
                  ],

                ),
              )),

          backgroundColor: Color(0XFF172530),

        ),

        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 210.0,left: 10.0),
                child: Text(
                  'Derniers ordres terminés',
                  style: TextStyle(fontSize: 14,fontFamily: "Poppins-regular",color: Color(0XFF222222)),
                ),
              ),
              SizedBox(height: 20,),
              TransactionCards(
                status: 'Validé',
                statusColor: Color(0XFF479444), // La couleur que vous voulez pour le statut "En attente"
                transactionDetails: 'CIV12345123401234567890101',
                name: 'Bridge Bank',
                amount: '1.000.000 FCFA',
                date: '12 Juillet 2023',
              ),
              TransactionCardFirst(
                status: 'Rejeté',
                statusColor: Color(0XFFC6311A), // La couleur que vous voulez pour le statut "En attente"
                transactionDetails: 'CIV12345123401234567890101',
                name: 'Bridge Bank',
                amount: '1.000.000 FCFA',
                date: '12 Juillet 2023',
              ),
              TransactionCardFirst(
                status: 'Rejeté',
                statusColor: Color(0XFFC6311A), // La couleur que vous voulez pour le statut "En attente"
                transactionDetails: 'CIV12345123401234567890101',
                name: 'Bridge Bank',
                amount: '1.000.000 FCFA',
                date: '12 Juillet 2023',
              ),
              TransactionCards(
                status: 'Validé',
                statusColor: Color(0XFF479444), // La couleur que vous voulez pour le statut "En attente"
                transactionDetails: 'CIV12345123401234567890101',
                name: 'Bridge Bank',
                amount: '1.000.000 FCFA',
                date: '12 Juillet 2023',
              ),
              TransactionCards(
                status: 'Validé',
                statusColor: Color(0XFF479444), // La couleur que vous voulez pour le statut "En attente"
                transactionDetails: 'CIV12345123401234567890101',
                name: 'Bridge Bank',
                amount: '1.000.000 FCFA',
                date: '12 Juillet 2023',
              ),

            ],
          ),
        ),
      ),
    );
  }
}

class TransactionCard extends StatelessWidget {
  final String status;
  final Color statusColor;
  final String transactionDetails;
  final String amount;
  final String name;
  final String date;

  const TransactionCard({
    Key? key,
    required this.status,
    required this.statusColor,
    required this.transactionDetails,
    required this.amount,
    required this.date,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Card(
      color: Color(0XFFFFFFFF),
      elevation: 1,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      StatusButton(status: status, color: statusColor),
                      SizedBox(width: screen.width*0.05,),
                      const CircleAvatar(
                        radius: 5,
                        backgroundColor: Color(0XFF2D75ED),
                      ),
                      SizedBox(width: screen.width*0.02,),
                      StatusButtons(onPressed: (){
                        print("J'ai cliqué");
                      })
                    ],
                  ),
                  SizedBox(height: 14),
                  Text(transactionDetails, style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
                  SizedBox(height: 14),
                  Row(
                    children: [
                      Expanded(child: Text(name, style: TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular") ,)),
                      Text(
                          amount,
                          style:TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular")),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(date, textAlign: TextAlign.end,style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
            ),
          ],
        ),
      ),
    );
  }
}
class TransactionCards extends StatelessWidget {
  final String status;
  final Color statusColor;
  final String transactionDetails;
  final String amount;
  final String name;
  final String date;

  const TransactionCards({
    Key? key,
    required this.status,
    required this.statusColor,
    required this.transactionDetails,
    required this.amount,
    required this.date,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Card(
      color: Color(0XFFFFFFFF),
      elevation: 1,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      StatusButton(status: status, color: statusColor),
                      SizedBox(width: screen.width*0.05,),
                      const CircleAvatar(
                        radius: 5,
                        backgroundColor: Color(0XFF479444),
                      ),
                      SizedBox(width: screen.width*0.02,),
                      StatusButtonsFirst(onPressed: (){
                        print("J'ai cliqué");
                      })
                    ],
                  ),
                  SizedBox(height: 14),
                  Text(transactionDetails, style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
                  SizedBox(height: 14),
                  Row(
                    children: [
                      Expanded(child: Text(name, style: TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular") ,)),
                      Text(
                          amount,
                          style:TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular")),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(date, textAlign: TextAlign.end,style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
            ),
          ],
        ),
      ),
    );
  }
}
class TransactionCardFirst extends StatelessWidget {
  final String status;
  final Color statusColor;
  final String transactionDetails;
  final String amount;
  final String name;
  final String date;

  const TransactionCardFirst({
    Key? key,
    required this.status,
    required this.statusColor,
    required this.transactionDetails,
    required this.amount,
    required this.date,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Card(
      color: Color(0XFFFFFFFF),
      elevation: 1,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      StatusButton(status: status, color: statusColor),
                      SizedBox(width: screen.width*0.05,),
                      const CircleAvatar(
                        radius: 5,
                        backgroundColor: Color(0XFFC6311A),
                      ),
                      SizedBox(width: screen.width*0.02,),
                      StatusButtonsFirst(onPressed: (){
                        print("J'ai cliqué");
                      })
                    ],
                  ),
                  SizedBox(height: 14),
                  Text(transactionDetails, style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
                  SizedBox(height: 14),
                  Row(
                    children: [
                      Expanded(child: Text(name, style: TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular") ,)),
                      Text(
                          amount,
                          style:TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular")),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(date, textAlign: TextAlign.end,style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
            ),
          ],
        ),
      ),
    );
  }
}

class StatusButton extends StatelessWidget {
  final String status;
  final Color color;

  const StatusButton({
    Key? key,
    required this.status,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.horizontal(),
      ),
      child: Text(
        status,
        style: TextStyle(
          color: Colors.white,
          fontFamily: "Poppins-regular",
        ),
      ),
    );
  }
}

class StatusButtons extends StatelessWidget {
  final VoidCallback onPressed;

  StatusButtons({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: const Text(
        'Ordre transmis',
        style: TextStyle(
          color: Color(0XFF222222),
          fontFamily: "Poppins-regular",
          // La graisse du texte
        ),
      ),
    );
  }
}
class StatusButtonsFirst extends StatelessWidget {
  final VoidCallback onPressed;

  StatusButtonsFirst({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: const Text(
        'Ordre terminé',
        style: TextStyle(
          color: Color(0XFF222222),
          fontFamily: "Poppins-regular",
          // La graisse du texte
        ),
      ),
    );
  }
}
class StatusButtonsSecond extends StatelessWidget {
  final VoidCallback onPressed;

  StatusButtonsSecond({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: const Text(
        'Ordre transmis',
        style: TextStyle(
          color: Color(0XFF222222),
          fontFamily: "Poppins-regular",
          // La graisse du texte
        ),
      ),
    );
  }
}
