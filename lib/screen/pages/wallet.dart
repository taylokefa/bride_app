import 'package:flutter/material.dart';

import '../Dashboard/historique.dart';

class ScreenWalletPage extends StatefulWidget {
  const ScreenWalletPage({super.key});

  @override
  State<ScreenWalletPage> createState() => _ScreenWalletPageState();
}

class _ScreenWalletPageState extends State<ScreenWalletPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        title: Image.asset(
            'assets/images/220315_BRIDGE_SECURITIES_LOGO_RGB-copy@3x.png',
            height: 50), // Assurez-vous que l'asset est dans votre projet
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_alert_rounded, color: Colors.white),
            onPressed: () {},
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          //padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              BalanceSection(),
              //TransactionButtons(),
              TransactionHistory(),
            ],
          ),
        ),
      ),
    );
  }
}

class BalanceSection extends StatelessWidget {
  bool _isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: screen.height / 2.5,
            color: Colors.black,
            child: Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Votre solde',
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Color(0XFFF1F5F8)),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          '1.000.000 FCFA',
                          style: TextStyle(
                              fontSize: 26,
                              fontWeight: FontWeight.bold,
                              color: Color(0XFFFFFFFF)),
                        ),
                        SizedBox(width: screen.width * 0.1),
                        IconButton(
                          icon: Icon(
                            _isPasswordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                          onPressed: () {
                            setState(() {
                              _isPasswordVisible = !_isPasswordVisible;
                            });
                          },
                        ),
                      ],
                    ),


                    const SizedBox(
                        height: 40), // Espace entre le texte et les boutons
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        _buildActionButton(
                            context, Icons.punch_clock_outlined, "Débiter"),
                        _buildActionButton(context, Icons.wallet, 'Créditer'),
                      ],
                    ),
                  ]),
            ),
          ),
        ],
      ),
    );
  }

  void setState(Null Function() param0) {}
}

Widget _buildActionButton(BuildContext context, IconData icon, String title) {
  return Column(
    children: [
      Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.grey[800],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Icon(icon, color: Colors.white, size: 30),
      ),
      const SizedBox(height: 8),
      Text(title, style: TextStyle(color: Colors.grey[600])),
    ],
  );
}

class TransactionHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              children: [
                const Text(
                  'Historique des transferts',
                  style: TextStyle(fontSize: 14.0,color: Color(0XFF222222),fontFamily: "Poppins-regular"),
                ),
                SizedBox(width: screen.width*0.28),
                TextButton(
                    style: ButtonStyle(),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ScreenHistorique()));
                    },
                    child: const Text("Voir plus",
                    style: TextStyle(
                      color: Color(0XFF172530),
                      fontFamily: "Poppins-regular"
                    ),))
              ],
            ),
          ),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem1('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem1('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem1('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem1('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),
          TransactionListItem('0707010203', '32,080.00 FCFA','10:23'),



          // Ajoutez d'autres éléments de liste ici
        ],
      ),
    );
  }
}

class TransactionListItem extends StatelessWidget {
  final String transactionType;
  final String amount;
  final String additionalSubtitle;

  TransactionListItem(this.transactionType, this.amount,this.additionalSubtitle);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        transactionType,
        style: const TextStyle(fontSize: 16,color: Color(0XFF172530),fontFamily: "Poppins_regular",fontWeight: FontWeight.bold),
      ),
      subtitle: const Text('TRANSFERT DE FONDS',
        style: TextStyle(fontSize: 12,color: Color(0XFF172530), fontFamily: "Poppins-light"),),

      trailing: Column(
        children: [
          Text(
            amount,
            style: const TextStyle(fontSize: 14,color: Colors.green, fontWeight: FontWeight.bold,fontFamily: "Poppins_regular"),
          ),
          Text(
            additionalSubtitle, // Affichage du sous-titre supplémentaire en dessous du montant
            style: TextStyle(fontSize: 12,color: Color(0XFF172530), fontFamily: "Poppins-light"), // Vous pouvez personnaliser le style comme vous voulez
          ),
        ],
      ),
    );
  }
}

class TransactionListItem1 extends StatelessWidget {
  late final String transactionTypes;
  late final String amounts;
  final String additionalSubtitles;

  TransactionListItem1(this.transactionTypes, this.amounts,this.additionalSubtitles);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        transactionTypes,
        style: const TextStyle(fontSize: 16,color: Color(0XFF172530),fontFamily: "Poppins_regular",fontWeight: FontWeight.bold),
      ),
      subtitle: const Text('RETRAIT DE FONDS',
        style: TextStyle(fontSize: 12,color: Color(0XFF172530), fontFamily: "Poppins-light"),
      ),
      trailing: Column(
        children: [
          Text(
              amounts,
              style: const TextStyle(fontSize: 14,color: Colors.red, fontWeight: FontWeight.bold,fontFamily: "Poppins_regular")          ),
          Text(
            additionalSubtitles, // Affichage du sous-titre supplémentaire en dessous du montant
            style: TextStyle(fontSize: 12,color: Color(0XFF172530), fontFamily: "Poppins-light"), // Vous pouvez personnaliser le style comme vous voulez
          ),
        ],
      ),
    );
  }
}

