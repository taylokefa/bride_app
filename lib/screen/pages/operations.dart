import 'package:bridge_app/screen/Dashboard/dashboard_real.dart';
import 'package:bridge_app/screen/Dashboard/historique.dart';
import 'package:bridge_app/screen/Dashboard/historique_virement.dart';
import 'package:bridge_app/screen/pages/detail_virement.dart';
import 'package:flutter/material.dart';

class ScreenOperationPage extends StatefulWidget {
  const ScreenOperationPage({super.key});

  @override
  State<ScreenOperationPage> createState() => _ScreenOperationPageState();
}

class _ScreenOperationPageState extends State<ScreenOperationPage> {
  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        title: Image.asset(
            'assets/images/220315_BRIDGE_SECURITIES_LOGO_RGB-copy@3x.png',
            height: 50), // Assurez-vous que l'asset est dans votre projet
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_alert, color: Colors.white),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.account_circle, color: Colors.white),
            onPressed: () {},
          ),
        ],
      ),
      body: ListView(
        children:  [
          const SizedBox(height: 30),
          ListTile(
            leading: Icon(Icons.money_sharp),
            title: Text('Demande d\'ordre de virement'),
            trailing: Icon(Icons.add),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const ScreenDashboardRealPage()));
            },
          ),
          const SizedBox(height: 12),
          ListTile(
            leading: Icon(Icons.money_sharp),
            title: Text('Demande par chèque'),
            trailing: Icon(Icons.add),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const ScreenDashboardRealPage()));
            },
          ),
          const SizedBox(height: 10),
             Column(
            children: [
              Padding(
                padding: EdgeInsets.only(right:200.0),
                child: Text(
                  'Derniers ordres transmis',
                  //textAlign: TextAlign.start,
                  style: TextStyle(fontSize: 14,fontFamily: "Poppins-regular",color: Color(0XFF222222)),
                ),
              ),
              SizedBox(height: 10),
              TransactionCard(
                status: 'En attente',
                statusColor: Color(0XFF2D75ED), // La couleur que vous voulez pour le statut "En attente"
                transactionDetails: 'CIV12345123401234567890101',
                name: 'Bridge Bank',
                amount: '1.000.000 FCFA',
                date: '12 Juillet 2023',
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 130.0,left: 20.0),
                    child: Text(
                      'Derniers ordres terminés',
                      style: TextStyle(fontSize: 14,fontFamily: "Poppins-regular",color: Color(0XFF222222)),
                    ),
                  ),
                  TextButton(
                      style: ButtonStyle(),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const ScreenHistoriqueVirementPage()));
                      },
                      child: const Text("Voir plus",
                        style: TextStyle(
                            color: Color(0XFF172530),
                            fontFamily: "Poppins-regular"
                        ),)),
                ],
              ),
              SizedBox(height: 10),
              TransactionCards(
                status: 'Validé',
                statusColor: Color(0XFF479444), // La couleur que vous voulez pour le statut "En attente"
                transactionDetails: 'CIV12345123401234567890101',
                name: 'Bridge Bank',
                amount: '1.000.000 FCFA',
                date: '12 Juillet 2023',
              ),
              TransactionCardFirst(
                status: 'Rejeté',
                statusColor: Color(0XFFC6311A), // La couleur que vous voulez pour le statut "En attente"
                transactionDetails: 'CIV12345123401234567890101',
                name: 'Bridge Bank',
                amount: '1.000.000 FCFA',
                date: '12 Juillet 2023',
              ),


            ],
          )
        ],
      ),
    );
  }
}

/*class StatusCard extends StatelessWidget {
  final String titreDemande;
  final String detailDemande;
  final String statut;
  final String date;
  final double montant;
  final Color couleurStatut;
  final IconData iconeStatut;

  StatusCard({
    required this.titreDemande,
    required this.detailDemande,
    required this.statut,
    required this.date,
    required this.montant,
    required this.couleurStatut,
    required this.iconeStatut,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: couleurStatut,
          child: Icon(iconeStatut, color: Colors.white),
        ),
        title: Text(titreDemande, style: TextStyle(fontWeight: FontWeight.bold)),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(detailDemande),
            SizedBox(height: 8),
            Text(date),
          ],
        ),
        trailing: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
          decoration: BoxDecoration(
            color: couleurStatut, // La couleur variera selon le statut
            borderRadius: BorderRadius.circular(20),
          ),
          child: Text(
            statut,
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}*/
class TransactionCard extends StatelessWidget {
  final String status;
  final Color statusColor;
  final String transactionDetails;
  final String amount;
  final String name;
  final String date;

  const TransactionCard({
    Key? key,
    required this.status,
    required this.statusColor,
    required this.transactionDetails,
    required this.amount,
    required this.date,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Card(
      color: Color(0XFFFFFFFF),
      elevation: 1,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      StatusButton(status: status, color: statusColor),
                      SizedBox(width: screen.width*0.05,),
                      const CircleAvatar(
                        radius: 5,
                        backgroundColor: Color(0XFF2D75ED),
                      ),
                      SizedBox(width: screen.width*0.02,),
                      StatusButtons(onPressed: (){

                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const ScreenDetailVirement()));
                      })
                    ],
                  ),
                  SizedBox(height: 14),
                  Text(transactionDetails, style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
                  SizedBox(height: 14),
                  Row(
                    children: [
                      Expanded(child: Text(name, style: TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular") ,)),
                      Text(
                          amount,
                          style:TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular")),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(date, textAlign: TextAlign.end,style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
            ),
          ],
        ),
      ),
    );
  }
}
class TransactionCards extends StatelessWidget {
  final String status;
  final Color statusColor;
  final String transactionDetails;
  final String amount;
  final String name;
  final String date;

  const TransactionCards({
    Key? key,
    required this.status,
    required this.statusColor,
    required this.transactionDetails,
    required this.amount,
    required this.date,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Card(
      color: Color(0XFFFFFFFF),
      elevation: 1,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      StatusButton(status: status, color: statusColor),
                      SizedBox(width: screen.width*0.05,),
                      const CircleAvatar(
                        radius: 5,
                        backgroundColor: Color(0XFF479444),
                      ),
                      SizedBox(width: screen.width*0.02,),
                      StatusButtonsFirst(onPressed: (){
                        print("J'ai cliqué");
                      })
                    ],
                  ),
                  SizedBox(height: 14),
                  Text(transactionDetails, style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
                  SizedBox(height: 14),
                  Row(
                    children: [
                      Expanded(child: Text(name, style: TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular") ,)),
                      Text(
                          amount,
                          style:TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular")),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(date, textAlign: TextAlign.end,style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
            ),
          ],
        ),
      ),
    );
  }
}
class TransactionCardFirst extends StatelessWidget {
  final String status;
  final Color statusColor;
  final String transactionDetails;
  final String amount;
  final String name;
  final String date;

  const TransactionCardFirst({
    Key? key,
    required this.status,
    required this.statusColor,
    required this.transactionDetails,
    required this.amount,
    required this.date,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Card(
      color: Color(0XFFFFFFFF),
      elevation: 1,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      StatusButton(status: status, color: statusColor),
                      SizedBox(width: screen.width*0.05,),
                      const CircleAvatar(
                        radius: 5,
                        backgroundColor: Color(0XFFC6311A),
                      ),
                      SizedBox(width: screen.width*0.02,),
                      StatusButtonsFirst(onPressed: (){
                        print("J'ai cliqué");
                      })
                    ],
                  ),
                  SizedBox(height: 14),
                  Text(transactionDetails, style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
                  SizedBox(height: 14),
                  Row(
                    children: [
                      Expanded(child: Text(name, style: TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular") ,)),
                      Text(
                          amount,
                          style:TextStyle(color: Color(0XFFAAAAAA),fontFamily: "Poppins-regular")),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(date, textAlign: TextAlign.end,style: TextStyle(fontFamily: "Poppins-regular",color: Color(0XFF222222))),
            ),
          ],
        ),
      ),
    );
  }
}

class StatusButton extends StatelessWidget {
  final String status;
  final Color color;

  const StatusButton({
    Key? key,
    required this.status,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.horizontal(),
      ),
      child: Text(
        status,
        style: TextStyle(
          color: Colors.white,
          fontFamily: "Poppins-regular",
        ),
      ),
    );
  }
}

class StatusButtons extends StatelessWidget {
  final VoidCallback onPressed;

  StatusButtons({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: const Text(
        'Ordre transmis',
        style: TextStyle(
          color: Color(0XFF222222),
          fontFamily: "Poppins-regular",
          // La graisse du texte
        ),
      ),
    );
  }
}
class StatusButtonsFirst extends StatelessWidget {
  final VoidCallback onPressed;

  StatusButtonsFirst({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: const Text(
        'Ordre terminé',
        style: TextStyle(
          color: Color(0XFF222222),
          fontFamily: "Poppins-regular",
          // La graisse du texte
        ),
      ),
    );
  }
}
class StatusButtonsSecond extends StatelessWidget {
  final VoidCallback onPressed;

  StatusButtonsSecond({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: const Text(
        'Ordre transmis',
        style: TextStyle(
          color: Color(0XFF222222),
          fontFamily: "Poppins-regular",
          // La graisse du texte
        ),
      ),
    );
  }
}

